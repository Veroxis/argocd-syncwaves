# ArgoCD local test in k3d

## Prerequisites

### create cluster

```sh
k3d cluster create k3d-argocd-test-cluster
```

### install argocd

```sh
# install argocd
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
kubectl -n argocd patch configmaps argocd-cm --patch-file resources/argocd-health-assessment.patch.yaml

# name: admin
# password:
kubectl get secret -n argocd argocd-initial-admin-secret -o json | jq -r '.data|to_entries|map({key, value:.value|@base64d})|from_entries["password"]'

# access the web ui
kubectl port-forward --address ::1 svc/argocd-server -n argocd 8081:80

# apply app-of-apps
kubectl apply -f argo-apps.yaml
```

## Sync-Wave example from ArgoCD examples

https://github.com/argoproj/argocd-example-apps/blob/master/sync-waves/manifests.yaml
